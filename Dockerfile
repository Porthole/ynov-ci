FROM ubuntu:latest
RUN apt-get update && apt-get install --yes curl
RUN mkdir -p /opt/script
WORKDIR /opt/script
COPY script.sh .
RUN chown root:root /opt/script/script.sh
RUN chmod 777 /opt/script/script.sh
